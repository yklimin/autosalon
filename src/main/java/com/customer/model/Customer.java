package com.customer.model;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.Objects;
import javax.persistence.Entity;

@Entity
@Embeddable
@Table(name = "customer")
public class Customer {
    @Id
    @Column(name = "idcustomer")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private BigInteger idcustomer;
    @Column(name = "fullname")
    private String fullname;
    @Column(name = "city")
    private String city;
    @Column(name = "address")
    private String address;
    @Column(name = "telephone")
    private String telephone;
    @Column(name = "passport")
    private String passport;
    @Column(name = "email")
    private String email;

    //get и set Id покупателя
    public BigInteger getIdcustomer() { return idcustomer; }
    public void setIdcustomer(BigInteger idcustomer) {
        this.idcustomer = idcustomer;
    }

    //get и set ФИО
    public String getFullname() {
        return fullname;
    }
    public void setFullname(String fullname) { this.fullname = fullname; }

    //get и set города
    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }

    //get и set адресса
    public String getAddress() { return address; }
    public void setAddress(String address) {
        this.address = address;
    }

    //get и set телефона
    public String getTelephone(String telephone) { return telephone; }
    public void setTelephone(String telephone) {this.telephone = telephone;}

    //get и set пасспорта
    public String getPassport() {return passport;}
    public void setPassport(String passport) {this.passport = passport;}

    //get и set email
    public String getEmail() {return email;}
    public void setEmail(String email) {this.email = email;}

    public Customer(String fullname, String city, String address, String telephone, String passport, String email) {
        this.fullname = fullname;
        this.city = city;
        this.address = address;
        this.telephone = telephone;
        this.passport = passport;
        this.email = email;
    }

    public Customer() {
    }

    @Override
    public String toString() {
        return "Customers{" +
                "idcustomer=" + idcustomer +
                ", fullname='" + fullname + '\'' +
                ", city='" + city + '\'' +
                ", address='" + address + '\'' +
                ", telephone='" + telephone + '\'' +
                ", passport='" + passport + '\'' +
                ", email='" + email + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Customer)) return false;
        Customer custom = (Customer) o;
        return Objects.equals(getIdcustomer(), custom.getIdcustomer());
    }

    @Override
    public int hashCode() {
        int result = idcustomer.hashCode();
        result = 31 * result + fullname.hashCode();
        result = 31 * result + city.hashCode();
        result = 31 * result + address.hashCode();
        result = 31 * result + telephone.hashCode();
        result = 31 * result + passport.hashCode();
        result = 31 * result + email.hashCode();
        return result;
    }
}
