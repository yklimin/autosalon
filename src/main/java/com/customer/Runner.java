package com.customer;

import com.customer.model.Customer;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.junit.Test;
import java.util.List;

public class Runner {
    @Test
    public void crud(){
        SessionFactory sessionFactory = new AnnotationConfiguration().configure().buildSessionFactory();
        Session session = sessionFactory.openSession();
        //delete(session);
        //uppdate(session);
        read(session);
        //create(session);
        session.close();
    }

    private void read(Session session){
        List<Customer> customersList = session.createQuery("SELECT s FROM Customer s").list();
        for(Customer s: customersList){
            System.out.println(("List:" + s));
        }
    }

    private void create(Session session) {
        Customer custom = new Customer();
        custom.setFullname("Бородина Т.А,");
        custom.setCity("Санкт-Петерубрг");
        custom.setAddress("пр. Ленина-72");
        custom.setTelephone("89101233685");
        custom.setPassport("2219 326585");
        custom.setEmail("borodina@yandex.ru");
        session.beginTransaction();
        session.save(custom);
        session.getTransaction().commit();
    }

    private void delete(Session session)
    {
        Query query = session.createQuery("DELETE FROM Customer WHERE idcustomer = '5'");
        query.executeUpdate();
    }

    private void uppdate(Session session){
        Query query = session.createQuery("update Customer s set s.city='St. Peterburg' where idcustomer = '5'");
        query.executeUpdate();
    }
}
